import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail



sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):

    data = json.loads(body)

    email = data["presenter_email"]
    title = data["title"]
    name = data["presenter_name"]

    send_mail(
        "Your presentation has been accepted",
        f"{name}, were happy to tell you that your presentation {title} has been accepted",
        "admin@conference.go",
        [email],
        fail_silently=False,
    )
    print("i am working")


def process_rejection(ch, method, properties, body):

    data = json.loads(body)

    email = data["presenter_email"]
    title = data["title"]
    name = data["presenter_name"]

    send_mail(
        "Your presentation has been rejected",
        f"{name}, were happy to tell you that your presentation {title} has been rejected",
        "admin@conference.go",
        [email],
        fail_silently=False,
    )
    print("i am working")
    
while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )

        print("we are consuming")
        channel.start_consuming()
    except AMQPConnectionError:
        time.sleep(2.0)

e